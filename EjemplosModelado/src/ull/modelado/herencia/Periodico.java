package ull.modelado.herencia;
//Clase periodico que hereda de ReferenciaBibliografica
public class Periodico extends ReferenciaBibliografica
{	//Atributos
	private String m_nombre_periodico;
	private int m_paginas;
	//Constructor/es y m�todos
	public Periodico(String a_titulo, String a_autor, int a_anio,
	        String a_nombre_periodico, int a_paginas)
	{
		super(a_titulo, a_autor, a_anio);
		m_nombre_periodico= a_nombre_periodico;
		m_paginas = a_paginas;
	}
	@Override
    public String mostrar()
    {
       return "Periodico->Titulo:"+getM_titulo()+", Autor:"+getM_autor()+", A�o:"+getM_anio();
    }
	//Getters & Setters
	public String getM_nombre_periodico()
	{
		return m_nombre_periodico;
	}
	public void setM_nombre_periodico(String m_nombre_periodico)
	{
		this.m_nombre_periodico = m_nombre_periodico;
	}
	public int getM_paginas()
	{
		return m_paginas;
	}
	public void setM_paginas(int m_paginas)
	{
		this.m_paginas = m_paginas;
	}
}
