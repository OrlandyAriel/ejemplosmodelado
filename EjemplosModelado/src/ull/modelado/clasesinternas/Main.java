package ull.modelado.clasesinternas;

public class Main
{
	public static void main(String[] args)
	{
		//CLASE INTERNA EST�TICA
		ExternaEstatica.EstaticaInterna t_aux = new ExternaEstatica.EstaticaInterna();
		//CLASE INTERNA MIEMBRO
		//ExternaMiembro.InternaMiembro t_aux1= new ExternaMiembro.InternaMiembro();
		/* esto no se puede hacer, para acceder a los m�todos de la clase 
		 * InternaMiembro se hace mediante la clase ExternaMiembro
		 * se deber�a hacer mediante alg�n m�todo que llame por nosotros
		 * al m�todo de la clase InternaMiembro.
		 */
		//ExternaMiembro.InternaMiembro t_aux1= new ExternaMiembro.InternaMiembro();
		ExternaMiembro t_aux1 = new ExternaMiembro();
		//CLASE INTERNA DE M�TODO
		ExternaDeMetodo t_aux2 = new ExternaDeMetodo();
		t_aux2.metodo();
		//CLASE INTERNA AN�NIMA
		Thread t_aux3 = new Thread(new Runnable()// implementa la interfaz Runneable que
				//que obliga a implementar la fuci�n run
		{
			@Override
			public void run()
			{
				System.out.println("an�nima");
			}
		});
		t_aux3.run();
	}
}
